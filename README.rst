pre-commit demo
===============

This repository contains and example on how to use pre-commit_ in your development flow.

Please remember this is just brief example and can contain nonsense code and comments inside ``.py`` files.
The goal is to provide experience using pre-commit_

Advantages:

* one file to define code standards
* Your project does not need CI/CD to comply with given standards.
* Once the pre-commit_ is configured it does not need many changes
* In your CI/CD lint stage you can run ``pre-commit run --all-files``
* pre-commit_ supports multiple languages

You can practically do what ever you desire with pre-commit_ plugins.

* Lint python, bash, go, ruby code (`supported languages <https://pre-commit.com/#supported-languages>`_)

* prettify or validate json and yaml

* process html tags for good practices

* and many more, just google ``pre-commit hook <your problem to solve>``

* Or write your own hook

This repository
---------------

There are 3 main points in this repository

* ``master`` branch - contains both examples of processed and non-processed ``.py`` files

  * original file: ``python-file.py``
  * pre-commit processed ``python_file.py``

* ``before-pre-commit`` tag - here you will try to use pre-commit on non-processed ``.py`` file.
* ``after-pre-commit`` tag - example of post processed ``.py`` with changes pointed by pre-commit_


Pre-commit tool contains set of hooks defined in ``.pre-commit-config.yaml``.
These hooks are executed with every run of  ``git commit``.


How to use this example:
------------------------

#. Create a new ``python`` virtual environment

#. Install all tools ``pip install pre-commit pylint``

#. Install pre-commit_ according to installation guide or check https://pre-commit.com/#install

#. Clone this repository

#. ``cd pre-commit-demo``

#. Switch to the ``before`` branch ``git checkout -b new-branch before-pre-commit``

#. Install ``pre-commit`` into the repository. ``pre-commit install``

#. Since we did not do any changes, we cannot actually commit, so let’s run pre-commit_ manually.
   ``pre-commit run --files files/python-file.py``

   * this step will install all hooks mentioned in ``.pre-commit-config.yaml``

#. Observe various complaints from hooks

#. Fix the file ``python-file.py`` and run the pre-commit_ again until all checks pass.

You can always compare your result with ``python_file.py`` - which passed pre-commit.

.. _pre-commit: http://pre-commit.com/
