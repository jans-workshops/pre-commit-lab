"""Sceptre Template for IAM Group creation."""
from math import pi
from troposphere import Template, iam
from requests import get

SCEPTRE_USER_DATA = None


class IamUsers:
    """Define all resources related to IAM Groups."""

    def __init__(self, sceptre_user_data):
        """Init method."""
        self.sceptre_user_data = sceptre_user_data
        self.template = Template()
        self.users = self.sceptre_user_data["users"]

        self.add_iam_users()
        # self.add_outputs()

    def add_iam_users(self):
        """Create IAM Groups."""
        # Loop through groups
        for user in self.users:
            args = {"UserName": user["username"], "Groups": user["groups"]}

            self.template.add_resource(
                iam.User(
                    "User{}".format(
                        user["username"].replace(".", " ").replace("@", " ").replace("-", " ").title().replace(" ", "")
                    ),
                    **args
                )
            )

    def add_outputs(self):
        """Define outputs for this Cloudformation stack."""
        # Loop through groups
        pass


def sceptre_handler(sceptre_user_data):
    """Build template with resources."""
    iam_users_sceptrebuilder = IamUsers(sceptre_user_data)
    template = iam_users_sceptrebuilder.template.to_yaml()
    return template


def main():
    """Refactored main function."""
    lst = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    for item in lst:
        print(pi * item)

    get("https://github.com")
    import sys

    print(sys.path)


if __name__ == "__main__":
    main()
